import fs from "fs";

const importFile = async (path, routeName) => {
  try {
    const route = await import(path);
    return [true, { routeName, route }];
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return [false, err];
  }
};

export const readFileRecursive = (promArr, basePath = "/") => {
  fs.readdirSync(`./src/routes${basePath}`, { withFileTypes: true }).forEach((file) => {
    if (file.isFile()) {
      if (!(file.name === "index.js" && basePath === "/")) {
        const fileName = file.name;
        const routeName = basePath + fileName.replace(".js", "");
        promArr.push(importFile(`../routes${basePath}${fileName}`, routeName));
      }
    }
    if (file.isDirectory()) {
      const baseName = file.name;
      readFileRecursive(promArr, `${basePath}${baseName}/`);
    }
  });
};

export const convertSecondsToTime = (secondsInDecimal) => {
  const seconds = Math.floor(secondsInDecimal);
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds - (hours * 3600)) / 60);
  const secs = seconds - (hours * 3600) - (minutes * 60);
  if (hours > 0) {
    return `${hours}h ${minutes}m ${secs}s`;
  }
  if (minutes > 0) {
    return `${minutes}m ${secs}s`;
  }
  return `${secs}s`;
};

export const successResponse = (res, data, status = 200) => {
  res.status(status).json({
    status: "SUCCESS",
    data,
  });
};

export * from "./jwt.js";
