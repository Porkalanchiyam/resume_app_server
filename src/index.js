import express from "express";
import cors from "cors";

import {
  __isProd__,
  __port__,
} from "./config.js";
import connectDb from "./db.js";
import inintializeRouter from "./routes/index.js";

const main = async () => {
  // Initialize db connection
  await connectDb();

  // Initialize express app
  const app = express();

  // Set up cors
  app.use(cors());

  // Parse the request body as JSON
  app.use(express.json());

  // Inintialize the router
  await inintializeRouter(app);

  // Error handling Middleware
  app.use((err, _, res, __) => {
    res.status(err.code || 500).json({
      status: "ERROR",
      message: err.message,
    });
  });

  // Start the server
  app.listen(__port__, () => {
    // eslint-disable-next-line no-console
    console.log(`Server is listening on port => ${__port__}`);
  });
};

// eslint-disable-next-line no-console
main().catch(console.error);
