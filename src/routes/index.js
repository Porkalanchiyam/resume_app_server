import {
  convertSecondsToTime,
  readFileRecursive,
  successResponse,
} from "../utils/index.js";

const inintializeRouter = async (app) => {
  const promArr = [];
  readFileRecursive(promArr);
  const data = await Promise.all(promArr);
  const isFailed = data.some((d) => !d[0]);
  if (isFailed) {
    throw new Error("Could not load all routes");
  }
  data.forEach(([, { routeName, route }]) => {
    app.use(`/api/v1${routeName}`, route.default);
  });

  app.all("/api/v1/ping", (req, res) => {
    const msg = `Server is running for ${convertSecondsToTime(process.uptime())}`;
    successResponse(res, msg);
  });

  app.all("*", (_, __, next) => {
    next({
      code: 404,
      message: "Not Found",
    });
  });
};

export default inintializeRouter;
