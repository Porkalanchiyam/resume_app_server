import express from "express";

import {
  addService,
  getServices,
  getSingleServices,
} from "../controller/service.js";

const router = express.Router();

router.post("/add", addService);

router.get("/", getServices);

router.get("/:id", getSingleServices);

export default router;
