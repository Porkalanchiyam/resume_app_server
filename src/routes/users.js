import express from "express";

import { loginController, signUpController } from "../controller/user.js";
import validationMiddleware from "../middlewares/validation.js";
import {
  loginSchema,
  signUpSchema,
} from "../Schema/index.js";

const router = express.Router();

router.post("/login", validationMiddleware(loginSchema), loginController);

router.post("/register", validationMiddleware(signUpSchema), signUpController);

export default router;
