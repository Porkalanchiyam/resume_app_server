import mongoose from "mongoose";

const ServiceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
}, {
  timestamps: {
    createdAt: true,
    updatedAt: true,
  },
});

const Service = mongoose.model("Service", ServiceSchema);

export default Service;
