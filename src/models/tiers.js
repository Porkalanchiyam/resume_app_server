import mongoose from "mongoose";

const TierSchema = new mongoose.Schema({
  service: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Service",
    required: true,
  },
  name: {
    type: String,
    required: true,

  },
  price: {
    symbol: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    term: {
      type: String,
      required: true,
      enum: ["month", "year"],
    },
  },
  actionName: {
    type: String,
    required: true,
  },
  features: {
    type: [String],
    required: true,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
}, {
  timestamps: {
    createdAt: true,
    updatedAt: true,
  },
});

const Tier = mongoose.model("Tier", TierSchema);

export default Tier;
