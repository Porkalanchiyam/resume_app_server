import argon2 from "argon2";
import mongoose from "mongoose";

import { signJwt } from "../utils/index.js";

const UserSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true,
    },
  },
);

UserSchema.pre("save", async function (next) {
  if (this.password) {
    const hashedPassword = await argon2.hash(this.password);
    this.password = hashedPassword;
  }

  next();
});

UserSchema.methods.comparePassword = async function (password) {
  if (!this.password) {
    return false;
  }
  const isValid = await argon2.verify(this.password, password);
  return isValid;
};

UserSchema.methods.generateAuthToken = async function () {
  const token = await signJwt({
    _id: this._id,
    name: this.name,
    email: this.email,
  });
  return token;
};

const User = mongoose.model("User", UserSchema);

export default User;
