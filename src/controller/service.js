import mongoose from "mongoose";

import { successResponse } from "../utils/index.js";
import Service from "../models/service.js";
import Tier from "../models/tiers.js";

export const addService = async (req, res, next) => {
  try {
    const {
      name,
      description,
      tiers,
    } = req.body;
    const service = await Service.create({
      name,
      description,
    });
    const tierData = tiers.map((tier) => ({
      service: service._id,
      name: tier.name,
      features: tier.features,
      price: tier.price,
      actionName: tier.actionName,
    }));
    await Tier.create(tierData);
    return successResponse(res, "Service added successfully", 201);
  } catch (err) {
    if (err.code === 11000) {
      return next({
        code: 409,
        message: "Service already exists",
      });
    }
    // eslint-disable-next-line no-console
    console.error(err);
    return next({
      code: 500,
      message: err.message || "Internal server error",
    });
  }
};

export const getServices = async (req, res, next) => {
  try {
    const services = await Service.find({
      isActive: true,
    });
    return successResponse(res, services, 200);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return next({
      code: 500,
      message: err.message || "Internal server error",
    });
  }
};

export const getSingleServices = async (req, res, next) => {
  try {
    const {
      id,
    } = req.params;
    const service = await Service.aggregate([
      {
        $lookup: {
          from: "tiers",
          localField: "_id",
          foreignField: "service",
          as: "tiers",
        },
      }, {
        $match: {
          _id: new mongoose.Types.ObjectId(id),
          isActive: true,
          "tiers.isActive": true,
        },
      },
    ]);
    if (service.length === 0) {
      return next({
        code: 404,
        message: "Service not found",
      });
    }
    const returnData = service[0];
    returnData.tiers.sort((a, b) => a.price.amount - b.price.amount);
    return successResponse(res, returnData, 200);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return next({
      code: 500,
      message: err.message || "Internal server error",
    });
  }
};
