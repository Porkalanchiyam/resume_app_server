import User from "../models/user.js";
import UserProfile from "../models/userProfile.js";
import Subscription from "../models/subscription.js";
import { successResponse } from "../utils/index.js";

export const loginController = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({
      email,
    });
    if (user) {
      if (!user.password) {
        return next({
          code: 401,
          message: "Please set your Password",
        });
      }
      const isValid = await user.comparePassword(password);
      if (isValid) {
        const token = await user.generateAuthToken();
        return successResponse(res, {
          user: {
            name: user.name,
            email: user.email,
            _id: user._id,
          },
          token,
        });
      }
    }
    return next({
      code: 404,
      message: "Invalid Email or Password",
    });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return next({
      code: 500,
      message: err.message || "Internal server error",
    });
  }
};

export const signUpController = async (req, res, next) => {
  try {
    const {
      firstName,
      lastName,
      email,
      address,
      country,
      state,
      zip,
      serviceID,
      tierID,
    } = req.body;
    const user = await User.create({
      firstName,
      lastName,
      email,
    });
    const userId = user._id;
    const userProfileProm = UserProfile.create({
      address,
      country,
      state,
      zip,
      user: userId,
    });
    const subscriptionProm = Subscription.create({
      user: userId,
      service: serviceID,
      tier: tierID,
    });
    await Promise.all([userProfileProm, subscriptionProm]);

    return successResponse(
      res,
      {
        user: {
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          _id: user._id,
        },
      },
      201,
    );
  } catch (err) {
    if (err.code === 11000) {
      return next({
        code: 409,
        message: "Email Address already exists",
      });
    }
    // eslint-disable-next-line no-console
    console.error(err);
    return next({
      code: 500,
      message: err.message || "Internal server error",
    });
  }
};
