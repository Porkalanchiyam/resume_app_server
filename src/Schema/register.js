import * as yup from "yup";

import { emailSchema } from "./baseFields.js";

// eslint-disable-next-line import/prefer-default-export
export const signUpSchema = emailSchema.concat(
  yup.object().shape({
    firstName: yup
      .string()
      .matches(/^[A-Za-z ]*$/, "Please enter valid name")
      .max(40)
      .required("Name is Required."),
    lastName: yup
      .string()
      .matches(/^[A-Za-z ]*$/, "Please enter valid name")
      .max(40)
      .required("Name is Required."),
    address: yup.string().required("Address is Required."),
    zip: yup
      .string()
      .required()
      .matches(/^[0-9]+$/, "Must be only digits")
      .min(5, "Must be at least 5 digits")
      .max(6, "Must be at most 6 digits"),
    country: yup.string().required("Country is Required."),
    state: yup.string().required("State is Required."),
    serviceID: yup.string().required(),
    tierID: yup.string().required(),
  }),
);
