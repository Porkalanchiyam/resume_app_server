# Getting Started

## Available Scripts

In the project directory, you can run:

For Development:

### `npm Install`

All the required Packages are installed.

### `npm run dev`

Runs the app in the development mode.\

For Production:

### Deployment

### `npm Install`

All the required Packages are installed.

### `npm start`

Optionally you can use PM2 to run the app in the production mode.
